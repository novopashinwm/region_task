﻿/*Имеем таблицу
  TBL
  (
  Date DateTime, --Дата
  Amount Money   --Сумма продаж
  )
Необходимо вывести нарастающий итог для продаж по дням (реализовать с наименьшим кол-вом запросов)
*/

--Сначала подготовим данные

declare @tbl table ([Date] DateTime, Amount Money)

insert @tbl values ('20100201',100)
            , ('20100301',101)
            , ('20100401',102)
            , ('20100501',101)

--Сам запрос
select t1.Date, sum(t2.Amount) as total_Amount
from @tbl t1
inner join @tbl t2 on (t2.Date <= t1.Date)
group by t1.Date
order by t1.Date