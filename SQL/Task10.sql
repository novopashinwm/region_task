﻿/*
Имеем таблицу
  TBL
  (
  ID Int,
  String VarChar(200)
  )
и произвольную строку @STR. Необходимо вывести следующую информацию в виде: [ID] [String] [Quantity] [Percent], где
[ID] [String] - поля таблицы TBL, в которых присутствуют совпадения со строкой @STR
[Quantity] количество встречающихся совпадений по каждой записи для строки @STR
[Percent]  процент количества совпадений для каждой записи от общего количества 
совпадений по всей таблице (реализовать с наименьшим кол-вом запросов)

*/

declare @TBL table (ID int, String varchar(200))
declare @STR varchar (400) = 'мыла'

INSERT INTO @TBL VALUES
 (1,'мама мыла раму')
,(2,'раму мыла мама')
,(3,'рама мама мало')
,(4, 'мама раму мыла и мыла и мыла...');


with cte as (
select [ID], [String], (len([String])-len(replace([String],@STR,'')))/len(@STR) as [Quantity]
from @TBL
where charindex(@STR, [String]) > 0)

select cte.[ID], [String], cast([Quantity] as float)/[Sum]*100 as [Percent]
from cte
inner join (select [ID], sum([Quantity]) as [Sum] from cte group by [ID]) t on t.[ID] = cte.[ID]