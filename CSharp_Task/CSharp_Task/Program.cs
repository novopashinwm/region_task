﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Task01();
            Task02( new int[] { 1, 2, 33 }, new int[] { 10, 20, 30, 40, 50, 60 });
            Task03();

            Console.WriteLine("Task 05");
            Task05(0);
            Task05(-1);

            Task06(new int[] { 3,1,2,4,6,7,8});
            Task08(5);
            Console.ReadKey();
        }


        /*3.	Объявлены две числовые переменные A и B. Необходимо сделать так, чтобы без объявления других переменных 
          в результате работы алгоритма переменная A приняла минимальное значение из A и B, а переменная B – максимальное. 
          Применять можно только операции присваивания, арифметические операции и любые стандартные 
          математические функции (использование операторов запрещено).
        
         */
        private static void Task03()
        {
            int A = 442, B = 313;

            Console.WriteLine($"Task03, Before A={A}, B={B}");
            A = A - B;
            B = Math.Max(B, A + B);
            A = Math.Min(A + B, B - A);
            Console.WriteLine($"After A={A}, B={B}");
            Console.WriteLine();
        }



        /*1.	Математический смысл функции:
        Функция Ф (А)
          Рез = 1;
          Для Инд = 2 По А Цикл
            Рез = Рез * Инд;
          КонецЦикла;
          Возврат Рез;
         КонецФункции
        */
        private static void Task01()
        {
            
            Console.WriteLine($"Task 01  F(10) = {F(10)}");
            Console.WriteLine();
        }

        static long F(int A) 
        {
            long res = 1;
            for (int i = 2; i <= A; i++)
            {
                res *= i;
            }
            return res;
        }

        /*
            2.	Дано два массива А[а] и В[b] (а и b – количества элементов массивов), упорядоченных по возрастанию. 
          Написать алгоритм, выдающий значения обоих массивов в порядке убывания за один цикл
         */
        private static void Task02(int[] A , int[] B)
        {
            Console.WriteLine("Task 02");
            int sum = A.Length + B.Length;
            int indexA = A.Length - 1, indexB = B.Length - 1;
            for (int i = 0; i < sum; i++)
            {
                if (indexB == -1 || A[indexA] > B[indexB])
                {
                    Console.Write($"{A[indexA--]} ");
                }
                else
                {
                    Console.Write($"{B[indexB--]} ");
                }
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        /*
            5.	Убрать условный оператор «Если» из следующего блока (А может принимать значения 0 или -1): 
            Если (А = 0) Тогда
              В = -1; 
            Иначе
              В = 10; 
            КонецЕсли; 
            Не допускается использование ЛЮБЫХ других условных операторов

         */
        static void Task05(int A) 
        {
            int B = -11 * A - 1;
            Console.WriteLine($"A={A}, B={B}");
        }

        /*6.	Определить число k, которое отсутствует в неупорядоченном массиве чисел (0, 1,...k-1,k+1,...n) за один цикл*/
        private static void Task06(int[] A)
        {
            Console.WriteLine("Task 06");
            int n = A.Length;
            int sumFull = (n + 1) * (n + 2) / 2;
            //В этом месте по сути Sum в Linq делает один цикл
            Console.WriteLine( sumFull - A.Sum());
        }

        /*8.	За один цикл вычислить значение функции F(n)=1!*2!*…*n! , где n! – факториал (1*2*…*n)*/
        static void Task08(int n) 
        {
            long multP = 1;
            long currFact = 1;
            for (int i = 1; i <= n; i++)
            {
                currFact *= i;
                multP = multP * currFact;
            }
            Console.WriteLine($"Task 08  Mult = {multP}");
        }
    }
}
